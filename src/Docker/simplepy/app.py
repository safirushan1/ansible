import time
import os

 # " duree d'endormissement"
duration = 5

# si la variable d'env est trouvee on ecrase la valeur de duration
if "SLEEP_DURATION" in os.environ:
    duration = int(os.environ[" SLEEP_DURATION"])

while True:
    print("simple app (python) in infinite loop...")
    time.sleep(duration)